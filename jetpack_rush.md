# Jetpack Rush: Privacy policy
Welcome to the Jetpack Rush for Android!

This is an offline Android game developed by Nightshadow Studios which is only available on Google Play.

As an avid Android user myself, I take privacy very seriously. I know how irritating it is when apps collect your data without your knowledge.

I have not programmed this game to collect any personal information. All data (app preferences (like theme, etc.) and alarms) created by the you is stored on your device only, and can be simply erased by clearing the app's data or uninstalling it.

We only require internet permission as we show Adsense ads.

If you find any security vulnerability that has been inadvertently caused by me, or have any question regarding how the app protectes your privacy, please send me an email and I will surely try to fix it/help you.

---


Yours sincerely,

Nightshadow Studios

contact.nightshadowstudios@gmail.com
